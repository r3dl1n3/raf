:- module(raf_gannic_92_squadron, []).

/** <module> RAF 92 Gannic squadron

Four sections of three Spitfires in Vic formation. 12 Spitfires in
total.

*/

squadron(gannic).

section(Section) :- member(Section, [red, yellow, blue, green]).

element(Element) :- between(1, 3, Element).

flight(red, a).
flight(yellow, a).
flight(blue, b).
flight(green, b).

section(Section0, Section) :-
    section(Section0),
    squadron(Squadron),
    atomic_list_concat([Squadron, Section0], '_', Section).

element(Section, Element0, Element) :-
    section(_, Section),
    element(Element0),
    atomic_list_concat([Section, Element0], '_', Element).

squadron_name(Squadron, "92 Sqn") :- squadron(Squadron).

section_name(Section, Name) :-
    section(Section0, Section),
    squadron(Squadron),
    squadron_name(Squadron, Name0),
    flight(Section0, Flight0),
    restyle_identifier('OneTwo', Flight0, Flight_),
    restyle_identifier('OneTwo', Section0, Section_),
    format(string(Name), '~s ~s-Flight ~s Section', [Name0, Flight_, Section_]).

element_name(Element, Name) :-
    element(Section, Element0, Element),
    section(Section0, Section),
    squadron(Squadron),
    squadron_name(Squadron, Name0),
    restyle_identifier('OneTwo', Section0, Section_),
    format(string(Name), '~s ~s-~d', [Name0, Section_, Element0]).

group(_World, Section, group{name:Name}) :- section_name(Section, Name).

:- listen(gannic:group(World, Section, Group), group(World, Section, Group)).
